package gui

import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.key.*
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import java.util.*

@OptIn(ExperimentalMaterialApi::class)
@Composable
@Preview
fun MainScreen(partida: Partida) {
    val joc = Joc()
    var grid = Array(6) { Array(5) { "" } }
    var intent = partida.intent
    var color = Array(6){Array(5){"gray"} }
    var encert :Boolean
    var paraulaAleatoria = joc.dic().random()
    println(paraulaAleatoria)

    var inputText by remember { mutableStateOf("") }
    val inputTextController = remember { TextFieldValue("") }

    MaterialTheme {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(50.dp),
            contentAlignment = Alignment.TopCenter
        ) {
            Text(
                text = "WORDLE",
                style = TextStyle(fontSize = 40.sp, fontWeight = FontWeight.Bold)
            )
        }

        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            for (i in 1 .. grid.size) {
                Row(
                    modifier = Modifier
                        .padding(2.dp)
                ) {
//                    color = partida.getColors("pomes", grid[intent].toString())
                    for (j in 1 .. grid[1].size) {
                        Box(
                            modifier = Modifier
                                .width(75.dp)
                                .height(75.dp)
                                .fillMaxSize()
                                .clip(shape = RoundedCornerShape(percent = 50))
                                .border(width = 2.dp, color = Color.Black, shape = RoundedCornerShape(percent = 50))
                                .background(joc.colors[color[i-1][j-1]]!!)
                        ) {
                            Text(grid[i-1][j-1],
                                modifier = Modifier.align(Alignment.Center),
                                fontSize = 20.sp,
                                fontWeight = FontWeight.Bold
                            )
                        }
                        Divider(modifier = Modifier.width(5.dp), color = Color.White)
                    }
                }
            }
            TextField(
                value = inputText.lowercase().trim(),
                onValueChange = { inputText = it },
                modifier = Modifier.padding(16.dp),
                label = { Text("Enter text to add to the grid") }
            )

            var showDialog by remember { mutableStateOf(false) }

            Column {
                // Aquí es defineixen els components de la pantalla, incloent el botó
                Button(
                    onClick = {
                        if (inputText.length == 5) {
                            for ((i, c) in inputText.uppercase().withIndex()) {
                                grid[intent][i] = c.toString()
                            }
                            color[intent] = partida.getColors(paraulaAleatoria, inputText.uppercase())

                            if ((color[intent].distinct().size == 1) && (color[intent][0] != "gray")) {
                                showDialog = true
                            }
                            intent++
                            inputText = ""
                        }
                    },
                    modifier = Modifier.padding(16.dp)
                ) {
                    Text("Add to grid")
                }
            }

            // Aquí es defineix l'AlertDialog
            if (showDialog) {
                AlertDialog(
                    onDismissRequest = { showDialog = false },
                    title = { Text("Has Guanyat!!!") },
                    text = { Text("Paraula Correcte") },
                    confirmButton = {
                        Button(onClick = { showDialog = false }) {
                            Text("OK")
                            grid = Array(6) { Array(5) { "" } }
                            intent = partida.intent
                            color = Array(6){Array(5){"gray"} }
                            encert = false
                            paraulaAleatoria = joc.dic().random()
                            println(paraulaAleatoria)
                        }
                    }
                )
            }
        }
    }
}

