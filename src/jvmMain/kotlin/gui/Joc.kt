package gui
import androidx.compose.ui.graphics.Color
import kotlin.io.path.*
import java.io.File
import java.nio.file.Path
import kotlin.io.path.*



class Joc {
    val colors: Map<String, Color> = mapOf(
        "green" to Color(108,169,101),
        "yellow" to Color(200,182,83),
        "gray" to Color(120,124,127),
        "black" to Color(0,0,0)
    )

    fun readData(file: Path): List<String> {
        return file.readLines()
    }



    fun dic(): Array<String> {
        val path = Path("./src/jvmMain/resources/cat.txt")
        val diccionariParaules =  readData(path).toTypedArray()
        var k = 0
        //passem tot el diccionari a majuscules
        while(k < diccionariParaules.size){
            diccionariParaules[k] = diccionariParaules[k].uppercase()
            k++
        }
        return diccionariParaules
    }

}