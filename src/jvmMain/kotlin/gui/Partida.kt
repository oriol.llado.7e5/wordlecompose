package gui

class Partida () {
    var intent = 0

    fun getColors(paraulaSecreta: String, paraulaIntro: String): Array<String>{
        var result: Array<String> = arrayOf()
        for((i, letter) in paraulaIntro.withIndex()){
            result += if (letter == paraulaSecreta[i]) "green"
            else if(letter in paraulaSecreta) "yellow"
            else "gray"
        }
        println(result.contentToString())
        return result
    }


}